-- AddForeignKey
ALTER TABLE `Transaction` ADD CONSTRAINT `Transaction_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `Wallet`(`user_id`) ON DELETE RESTRICT ON UPDATE CASCADE;
