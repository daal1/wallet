-- CreateTable
CREATE TABLE `Wallet` (
    `user_id` INTEGER NOT NULL,
    `balance` BIGINT NOT NULL,

    PRIMARY KEY (`user_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
