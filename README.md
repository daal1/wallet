## Description

Daal project

## Running the app

```bash
$ docker-compose up
```

## Running the unit tests

```bash
$ npm run test:watch
```

## Running the e2e tests
First run this command
```bash
$ npx prisma generate

```
And after that run this command

```bash
$ npm run test:e2e
```
**postman export file is located in root directory.**
