import { Injectable } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { TransactionService } from 'src/transaction/services/transaction.service';

@Injectable()
export class SchedulerService {
  constructor(private readonly transactionService: TransactionService) {}
  @Cron('* * */24 * * *')
  async handleCron() {
    await this.transactionService.calculateTotalBalanceTask();
  }
}
