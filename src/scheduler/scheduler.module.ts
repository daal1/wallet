import { Module } from '@nestjs/common';
import { SchedulerService } from './services/scheduler.service';
import { ScheduleModule } from '@nestjs/schedule';
import { TransactionModule } from 'src/transaction/transaction.module';

@Module({
  providers: [SchedulerService],
  imports: [ScheduleModule.forRoot(), TransactionModule],
})
export class SchedulerModule {}
