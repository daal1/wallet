import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WalletModule } from './wallet/wallet.module';
import { TransactionModule } from './transaction/transaction.module';
import { SchedulerModule } from './scheduler/scheduler.module';

@Module({
  imports: [WalletModule, TransactionModule, SchedulerModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
