import { Test, TestingModule } from '@nestjs/testing';
import { TransactionService } from './transaction.service';
import { WalletService } from 'src/wallet/services/wallet.service';
import { PrismaService } from 'src/prisma/services/prisma.service';
import { Wallet } from '@prisma/client';

describe('TransactionService', () => {
  let service: TransactionService;
  let wallets: Wallet[];

  const fakeWalletService = {
    findOrCreate: async ({ data }) => {
      let wallet = wallets.find((value) => {
        return value.user_id == data.user_id;
      });
      if (!wallet) {
        wallet = { user_id: data.user_id, balance: BigInt(data.amount) };
        wallets.push(wallet);
      }
      return wallet;
    },

    updateBalance: async ({ user_id, balance }) => {
      const wallet = wallets.find((value, index) => {
        if (value.user_id == user_id) {
          wallets[index] = { user_id, balance };
          return true;
        }
      });
      return wallet;
    },
  };

  const fakePrismaService = {
    wallet: {
      find: async ({ where }) => {
        return { user_id: where.user_id, balance: BigInt(0) };
      },
      update: async ({ data }) => data,
      create: async ({ data }) => data,
    },
    transaction: {
      create: async ({ data }) => {
        return {
          user_id: data.Wallet.connect.user_id,
          amount: BigInt(data.amount),
        };
      },
    },
    $transaction: async (func) => {
      await func(fakePrismaService);
      return Promise.resolve();
    },
  };

  beforeEach(async () => {
    wallets = [{ user_id: 1, balance: BigInt(0) }];

    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TransactionService,
        {
          provide: WalletService,
          useValue: fakeWalletService,
        },
        {
          provide: PrismaService,
          useValue: fakePrismaService,
        },
      ],
    }).compile();

    service = module.get<TransactionService>(TransactionService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('checking addTransaction function:', async () => {
    const transaction = await service.addTransaction({
      data: { amount: 5000, Wallet: { connect: { user_id: 1 } } },
    });
    expect(transaction).toBeDefined();
  });

  it('checking increasing balance work correctly:', async () => {
    await service.addTransaction({
      data: { amount: 5000, Wallet: { connect: { user_id: 1 } } },
    });

    expect(wallets[0].balance).toEqual(BigInt(5000));

    await service.addTransaction({
      data: { amount: 5000, Wallet: { connect: { user_id: 1 } } },
    });

    expect(wallets[0].balance).toEqual(BigInt(10000));
  });
});
