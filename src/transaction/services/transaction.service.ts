import { Injectable } from '@nestjs/common';
import { Transaction, Wallet } from '@prisma/client';
import { PrismaService } from 'src/prisma/services/prisma.service';
import { WalletService } from 'src/wallet/services/wallet.service';
import { CreateInput } from '../types/create-input.type';
import { AddTransactionInput } from '../types/add-transaction-input.type';

@Injectable()
export class TransactionService {
  constructor(
    private readonly prisma: PrismaService,
    private readonly walletService: WalletService,
  ) {}

  private async create({ data, prisma = this.prisma }: CreateInput) {
    return prisma.transaction.create({ data });
  }

  async addTransaction({ data }: AddTransactionInput) {
    let transaction: Transaction;

    await this.prisma.$transaction(async (prisma: PrismaService) => {
      const wallet = await this.walletService.findOrCreate({
        data: { user_id: data.Wallet.connect.user_id, balance: 0 },
        prisma,
      });

      transaction = await this.create({ data, prisma });

      const balance = transaction.amount + wallet.balance;

      await this.walletService.updateBalance({
        user_id: transaction.user_id,
        balance: balance,
        prisma,
      });
    });

    return transaction;
  }

  async calculateTotalBalanceTask() {
    const wallets: Wallet[] = await this.prisma
      .$queryRaw`SELECT user_id, SUM(amount) AS 'balance' FROM \`Transaction\` GROUP BY user_id`;
    wallets.map((wallet) => {
      console.log(
        `the balance of user_id ${wallet.user_id} is ${wallet.balance}`,
      );
    });
  }
}
