import { Body, Controller, Post } from '@nestjs/common';
import { TransactionService } from '../services/transaction.service';
import { CreateDto } from '../dtos/create.dto';

@Controller('transaction')
export class TransactionController {
  constructor(private readonly transactionService: TransactionService) {}

  @Post('')
  async create(@Body() body: CreateDto) {
    const transaction = await this.transactionService.addTransaction({
      data: {
        amount: body.amount,
        Wallet: { connect: { user_id: body.user_id } },
      },
    });

    return {
      reference_id: transaction.id,
    };
  }
}
