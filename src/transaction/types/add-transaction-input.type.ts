import { Prisma } from '@prisma/client';

export type AddTransactionInput = { data: Prisma.TransactionCreateInput };
