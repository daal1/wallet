import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/services/prisma.service';

export type CreateInput = {
  data: Prisma.TransactionCreateInput;
  prisma?: PrismaService;
};
