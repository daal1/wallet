import { Transform } from 'class-transformer';
import { IsNumber } from 'class-validator';

export class CreateDto {
  @Transform(({ value }) => Number(value))
  @IsNumber()
  user_id: number;

  @Transform(({ value }) => Number(value))
  @IsNumber()
  amount: bigint;
}
