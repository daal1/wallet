import { ValidationPipe } from '@nestjs/common';

export function setupApp(app: any) {
  app.useGlobalPipes(
    new ValidationPipe({
      whitelist: true,
    }),
  );
}
