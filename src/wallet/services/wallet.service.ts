import { Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma/services/prisma.service';
import { FindOneType } from '../types/find-one-input.type';
import { CreateInput } from '../types/create-input.type';
import { UpdateInput } from '../types/update-input.type';
import { UpdateBalanceInput } from '../types/update-balance-input';

@Injectable()
export class WalletService {
  constructor(private readonly prisma: PrismaService) {}

  async findOne({ where, prisma = this.prisma }: FindOneType) {
    return prisma.wallet.findUnique({ where });
  }

  async findOrCreate({ data, prisma = this.prisma }: CreateInput) {
    let wallet = await this.findOne({
      where: { user_id: data.user_id },
      prisma,
    });

    if (!wallet) wallet = await prisma.wallet.create({ data });

    return wallet;
  }

  private async update({ where, data, prisma = this.prisma }: UpdateInput) {
    return prisma.wallet.update({ where, data });
  }

  async updateBalance({
    user_id,
    balance,
    prisma = this.prisma,
  }: UpdateBalanceInput) {
    return await this.update({
      where: { user_id: user_id },
      data: { balance: balance, user_id: user_id },
      prisma,
    });
  }
}
