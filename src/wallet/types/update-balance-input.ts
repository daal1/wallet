import { PrismaService } from 'src/prisma/services/prisma.service';

export type UpdateBalanceInput = {
  user_id: number;
  balance: bigint;
  prisma?: PrismaService;
};
