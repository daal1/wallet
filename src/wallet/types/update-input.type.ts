import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/services/prisma.service';

export type UpdateInput = {
  data: Prisma.WalletCreateInput;
  where: Prisma.WalletWhereUniqueInput;
  prisma?: PrismaService;
};
