import { Prisma } from '@prisma/client';
import { PrismaService } from 'src/prisma/services/prisma.service';

export type FindOneType = {
  where: Prisma.WalletWhereUniqueInput;
  prisma?: PrismaService;
};
