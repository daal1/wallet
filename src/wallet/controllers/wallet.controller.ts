import { Controller, Get, Query } from '@nestjs/common';
import { WalletService } from '../services/wallet.service';
import { GetDto } from '../dtos/get.dto';

@Controller('wallet')
export class WalletController {
  constructor(private readonly walletService: WalletService) {}
  @Get('')
  async get(@Query() query: GetDto) {
    let wallet = await this.walletService.findOne({
      where: { user_id: query.user_id },
    });

    if (!wallet) wallet = { user_id: query.user_id, balance: BigInt(0) };

    //TODO: add interceptor to serialize response
    return { ...wallet, balance: wallet.balance.toString() };
  }
}
