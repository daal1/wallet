import { Module } from '@nestjs/common';
import { WalletService } from './services/wallet.service';
import { PrismaModule } from 'src/prisma/prisma.module';
import { WalletController } from './controllers/wallet.controller';

@Module({
  imports: [PrismaModule],
  providers: [WalletService],
  controllers: [WalletController],
  exports: [WalletService],
})
export class WalletModule {}
