import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { setupApp } from 'src/setup-app';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  const user_id = Math.floor(Math.random() * 1000);

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();

    setupApp(app);

    await app.init();
  });

  afterAll(async () => {
    await app.close();
  });

  it("/transaction (POST): adding 10000 to user's wallet", async () => {
    return request(app.getHttpServer())
      .post('/transaction')
      .send({
        user_id: user_id,
        amount: 10000,
      })
      .expect(201)
      .then((res) => {
        const { reference_id } = res.body;
        expect(reference_id).toBeDefined();
      });
  });

  it('/wallet (Get): checking working correctly', async () => {
    return request(app.getHttpServer())
      .get('/wallet')
      .query({ user_id })
      .expect(200)
      .then((res) => {
        const { user_id, balance } = res.body;
        expect(balance).toEqual('10000');
        expect(user_id).toEqual(user_id);
      });
  });

  it("/transaction (POST): adding -10000 to user's wallet", async () => {
    return request(app.getHttpServer())
      .post('/transaction')
      .send({
        user_id: user_id,
        amount: -10000,
      })
      .expect(201)
      .then((res) => {
        const { reference_id } = res.body;
        expect(reference_id).toBeDefined();
      });
  });

  it('/wallet (Get): checking calculate balance working correctly', async () => {
    return request(app.getHttpServer())
      .get('/wallet')
      .query({ user_id })
      .expect(200)
      .then((res) => {
        const { user_id, balance } = res.body;
        expect(balance).toEqual('0');
        expect(user_id).toEqual(user_id);
      });
  });
});
